![MasterHead](https://github.com/MahmoudYoussefSdiek/MahmoudYoussefSdiek/blob/main/coding.gif)

<h1 align="center"> Hi 👋, I'm Mahmoud Youssef </h1>

<!-- <h1 align="center">
  Hi <img src="https://github.com/MahmoudYoussefSdiek/MahmoudYoussefSdiek/blob/main/wave.gif" width="30px" height="30px"/>, I'm Mahmoud Youssef
</h1> -->
<!-- "https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" -->
<h3 align="center">Software Engineer | Flutter</h3>

## <picture><img src = "https://github.com/MahmoudYoussefSdiek/MahmoudYoussefSdiek/blob/main/Icons/about_me.gif" width = 50px></picture> About me
I graduated from Mansoura University in 2019 with a degree in Computer Science, where I gained a solid foundation in programming, mathematics, and problem-solving, I am developing my software engineering skills through a rigorous and hands-on curriculum that covers topics such as data structures, algorithms, web development, I am passionate about learning new technologies, collaborating with peers, and applying my knowledge to real-world challenges, 
My goal is to be one of the world's best software engineers, through knowledge, learning, hard-working and technical experience.


<img align="right" alt="Coding" width="400" src="https://github.com/MahmoudYoussefSdiek/MahmoudYoussefSdiek/blob/main/programming.gif">

## Profile Views

  <table>
    <tr>
      <th>Total Count</th>
    </tr>
    <tr>
      <td>
         <a href="https://github.com/mahmoudyoussefsdiek"> <img src="https://komarev.com/ghpvc/?username=mahmoudyoussefsdiek&label=Profile%20views&color=0e75b6&style=flat" alt="mahmoudyoussefsdiek"> </a>
      </td>
    </tr>
  </table>
<!--<p align="left"> <img src="https://komarev.com/ghpvc/?username=mahmoudyoussefsdiek&label=Profile%20views&color=0e75b6&style=flat" alt="mahmoudyoussefsdiek" /> -->

- 📫 How to reach me : **mahmoudyoussefsdiekmd@gmail.com**
- 🌱 I’m currently learning : Back-End with ALX program
- 📄 My CV : [Link](https://drive.google.com/file/d/1F7Fe8vLVsEfMnZtEyulZEEf86rLa-cWL/view?usp=sharing)
  
- ⚡ Fun fact : **Programming is fun 😂**

## Connect with me:
<p align="left">
<a href="https://linkedin.com/in/mahmoudyoussefsdiek" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="mahmoudyoussefsdiek" height="30" width="40" /></a>
</p>

## Languages and Tools:
<!--
<p align="center"> 

  <a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> 
  <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> 
  <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" 
  height="40"/> </a> 
  <a href="https://dart.dev" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/dartlang/dartlang-icon.svg" alt="dart" width="40" height="40"/> </a> 
  <a href="https://firebase.google.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/firebase/firebase-icon.svg" alt="firebase" width="40" height="40"/> </a> 
  <a href="https://flutter.dev" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" alt="flutter" width="40" height="40"/> </a> 
  <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> 
  <a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="40" height="40"/> </a> 
  <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> 
  <a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="40" height="40"/> </a> 
  <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> 
  </a> </p>
  -->

  <p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=c,cpp,dart,flutter,py,java,firebase,sqlite,ai,git,github,linux,bash,md,vim,vscode,androidstudio,linkedin,stackoverflow,discord"/>
  </a>
  </p>
  <p align="center">
    <!--  <img src="https://skillicons.dev/icons?i=vscode,replit,github,mongodb,css,html,js,express,bots,nodejs"> -->
  </p>

## My Projects
- App on Google Play : [Cinematic World](https://play.google.com/store/apps/details?id=com.MahmoudYoussef.Journey_To_Cinematic_World)

## My Stats:
<p align="center">
 <img height="180px" src="https://github-readme-stats-sigma-five.vercel.app/api/top-langs?username=mahmoudyoussefsdiek&hide_border=true&show_icons=true&locale=en&layout=compact&bg_color=151515" alt="mahmoudyoussefsdiek" />
 <img height="180px" src="https://github-readme-stats-sigma-five.vercel.app/api?username=mahmoudyoussefsdiek&hide_border=true&show_icons=true&locale=en&bg_color=151515" alt="mahmoudyoussefsdiek" />
<!--   <img height="180px" src="https://github-readme-stats.vercel.app/api?username=mahmoudyoussefsdiek&hide_border=true&show_icons=true&locale=en&bg_color=151515" alt="mahmoudyoussefsdiek" /> -->
</p>


## Activity On GitHub:
<p align="center">
 <img height="180px" src="https://github-readme-streak-stats.herokuapp.com/?user=mahmoudyoussefsdiek&theme=dark&hide_border=true&stroke=f53b3b" alt="mahmoudyoussefsdiek" />
</p>
